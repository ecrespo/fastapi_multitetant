from fastapi import FastAPI

from app.utils.loggerfactory import LoggerFactory
from app.api.tests import tests
import os


logger = LoggerFactory.get_logger("movie-service", log_level="INFO")

app = FastAPI(
    title="My App",
    debug=True,
    openapi_url="/api/v1/tests/openapi.json",
    docs_url="/api/v1/tests/docs",
)


@app.on_event("startup")
async def startup():
    logger.info("Starting up!")


@app.on_event("shutdown")
async def shutdown():
    logger.info("Shutdown!")


app.include_router(tests, prefix="/api/v1/tests", tags=["tests"])
