from fastapi import FastAPI

from app.utils.loggerfactory import LoggerFactory
from app.api.short_url import app_short_url

import os


logger = LoggerFactory.get_logger("movie-service", log_level="INFO")

app = FastAPI(
    title="Short urls app",
    debug=True,
    openapi_url="/api/v1/shorturl/openapi.json",
    docs_url="/api/v1/shorturl/docs",
)


@app.on_event("startup")
async def startup():
    logger.info("Starting up!")


@app.on_event("shutdown")
async def shutdown():
    logger.info("Shutdown!")


app.include_router(app_short_url, prefix="/api/v1/shorturl", tags=["shorturl"])
